<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function createProduct(ProductRequest $request)
    {
        try {

            $data = $request->validated();
            $dataProduct = Product::create($data);
            return response()->json([
                'status' => true,
                'data' => $dataProduct,
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function editProduct(ProductRequest $request, $id)
    {
        try {
            $data = $request->validated();
            $dataProduct = Product::findOrFail($id);
            $dataProduct->update($data);
            return response()->json([
                'status' => true,
                'data' => $dataProduct,
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function getProduct()
    {
        try {
            $dataProduct = Product::all();
            return response()->json([
                'status' => true,
                'data' => $dataProduct,
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function getIdProduct($id)
    {
        try {
            $dataProduct = Product::findOrFail($id);
            return response()->json([
                'status' => true,
                'data' => $dataProduct,
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }
    public function deleteProduct($id)
    {
        try {
            $dataProduct = Product::findOrFail($id);
            $dataProduct->delete();
            return response("xóa product thành công");
        } catch (\Exception $e) {
            return $e;
        }
    }
}
