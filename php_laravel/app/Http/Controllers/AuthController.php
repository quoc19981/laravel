<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
  public function login(LoginRequest $request)
  {
    try {
      $data = $request->validated();
      $user = User::email($data['email'])->first();
      if ($user) {
        if ($user->password ==  hash('md5', $data['password'])) {
          $token = $user->createToken('auth_token')->plainTextToken;
          $user['token'] = $token;
          return response()->json([
            'status' => true,
            'data' => $user,
          ]);
        } else {
          return response()->json([
            'status' => 403,
            'message' => 'mật khẩu sai',
          ], 403);
        }
      } else {
        return response()->json([
          'status' => 403,
          'message' => 'email sai',
        ], 403);
      }
    } catch (\Exception $e) {
      return response($e);
    }
  }
  public function register(RegisterRequest $request)
  {
    try {
      $data = $request->validated();
      $user = User::email($data['email'])->first();
      if ($user) {
        return response()->json([
          'status' => 403,
          'message' => 'email đã tồn tại',
        ], 403);
      } else {
        $dataUser = User::create([
          "email" => $data['email'],
          "password" => hash('md5', $data['password']),
          "name" => $data['name'],
        ]);
        return response()->json([
          'status' => true,
          'data' => $dataUser,
        ]);
      }
    } catch (\Exception $e) {
      return response($e);
    }
  }
}
